# detect_circle
Projet de Dorian VERRIERE
et Thibault DELORME  
(APP5 INFO,
rendu le 22/12/2019)

### Un cercle est décrit par:
 * un centre (r, c)
 * un rayon rad

On doit stocker toutes les valeurs discrétisées de r, δr correspond au pas d’acquisition, on a donc (la logique est la même pour c):
* pour δr = 2, r prend ((100-1)/2)=49 valeurs possibles
* pour δr = 0.5, r prend ((100-1)/0.5)=198 valeurs possibles



On doit stocker tous les centres des cercles possibles, chaque point discrétisé est le centre de cercles, en cas général, on stocke ((rmax - rmin)/δr)^2 valeurs de centres:

* pour δr = δc = 2, il y a 49^2=2401 centres
* pour δr = δc = 0.5, r prend 198^2=39204 centres
Ensuite, pour chaque centre, on doit stocker tous les cercles de différents rayon rad, rad peut prendre (radmax-radmin)/δrad valeurs
* pour radmax = 100*sqrt(2), radmin=5, et δrad = 1, on a (100*sqrt(2)-5)/1=136 rayons à stocker
Avec les différentes valeurs de r, c et rad, on peut stocket |r|x|c|x|rad| cercles
* dans l'exemple courant, on peut décrire 136*99*99=1332936 cercles

#### Le tableau tridimensionnel associé à la case acc(i,j,k) le cercle situe à la i-ème valeur discrète de r, la j-ème valeur discrète de c, et la k-ème valeur discrète de rad.

* acc(0,0,0) <=> cercle de centre (0,0), de rayon 0 = point origine du repère
* acc(9,6,29) <=> cercle de centre (10,7), de rayon 29
* le cercle centre dans le pixel (40,40) et de rayon rad= 13 correspond à acc(39,39,12)

____

## Compilation
~~~~
cmake ./CMakeLists.txt
make
~~~~

## Lancement
~~~~
./bin/detectCircle IMAGE_FILE [NB_CIRCLE=5] [MIN_RAD=4]

 - IMAGE_FILE: image où détecter les cercles
 - NB_CIRCLE: nombre de cercles à détecter (=5 par défaut)
 - MIN_RAD: rayon minimum des cercles à détecter (=4 par défaut)
~~~~

____

## Test
~~~~
./bin/detectCircle ./res/four.png         3.945 seconds
~~~~
<img src="./res/resultcoins2.png" width="300">

~~~~
./bin/detectCircle ./res/fourn.png 4      3.943 seconds
~~~~
<img src="./res/resultfourn.png" width="300">


~~~~
./bin/detectCircle ./res/coins.png 2      0.439 seconds
~~~~
<img src="./res/resultcoins.png" width="300">


~~~~
./bin/detectCircle ./res/coins2.jpg 8 30  928.445 seconds
~~~~
<img src="./res/resultfour.png" width="300">


~~~~
./bin/detectCircle ./res/MoonCoin.png 1   0.247 seconds
~~~~
<img src="./res/resultMoonCoin.png" width="300">


____

## Fonctionnement
#### 1/ Filtrages
Pour diminuer les effets d'un potentiel bruit sur la détection des contours, on commence par appliquer un filtre gaussien à l'image (chargé en niveau de gris).
Pour détecter les contours, on applique deux filtres de Sobel, pour détecter les changement brutaux de valeurs sur les axes horizontaux et verticaux (qui correspondent aux contours). On assemble les résultats (=magnitudes des gradients) des deux axes dans une même matrice *grad* (pour accélérer le traitement, un ne calcule pas la norme du vecteur, mais 1/2 x la première + 1/2 x la seconde matrice).
On obtient alors une matrice de dimension la taille de l'image, contenant des valeurs entières, ou les plus importantes correspondent aux valeurs des contours.

#### 2/ Enregistrement des cercles
Pour chaque valeurs de *grad*, si elle dépasse un certain seuil *border_threshold*, on considère ce pixel comme faisant partie d'une bordure, et possiblement de celle d'un cercle.

__Note:__ la valeur *border_threshold* peut dépendre de l'image traitée, nous avons essayé de la calculer en utilisant la valeur médiane entre la valeur max et la moyenne des valeurs de *grad*. Ce calcule est toujours disponible dans la fonction *getThreshold()*, mais le calcul était plutôt gourmand, et le résultat était toujours proche de la même valeur, nous avons donc fixé la valeur à 150.

Quand un pixel est détecté comme une bordure, on utilise la fonction *addAllCircles* pour enregistrer tous les cercles donc le pixel est susceptible d’être une bordure dans la matrice *circles*.

#### 3/ Identification des maximums locaux
A ce stade de développement, on commence à avoir des résultats cohérents, mais on remarque que les cercles observables sur l'images sont enregistrés plusieurs fois. Cela est du au fait que les bordures sont détectées plus épaisses qu'elles ne le sont vraiment, probablement à cause de la valeur fixe de *border_threshold*.
On utilise alors *cleanCircles* pour identifier les maximums locaux, et ainsi ne garde que les cercles avec les plus grandes valeurs dans un espace définit.

__Note:__ Pour les images grandes (>1000px), les cercles étaient toujours détectés plusieurs fois, nous avons donc décidé d'augmenté l'espace de recherche des maximums locaux, en passant de 26 à 120 valeurs. Ce choix ne semble pas affecter les résultats sur les petites images.

#### 4/ Sélection des meilleures détections
On dispose à présent d'une matrice représentant tous les cercles possiblement présents dans l'image, avec un compteur correspondant au nombre de fois où un pixel de bordure a fait partie de ce cercle.
On utilise la fonction *add_if_top()* pour conserver uniquement les *NB_CIRCLE* cercles les mieux détectés.

__Note:__ On observe logiquement que plus les cercles sont grands, plus ils ont été "marqués" dans la matrice *circles*, on applique alors un rapport de proportionnalité entre la valeur du compteur et le périmètre du cercle potentiellement détecté? Ce rapport est calculé avec la fonction:
```
float getRadProp(detected_circle c)
{
	return (c.score / (2.*M_PI*c.rad));
}
```
Ainsi on garantie qu'un petit cercle soit aussi bien détecté qu'un grand.

____

#### Temps de calcul
On observe que le temps de calcul devient vite très grand quand on traite des images des plus de 100000px. la complexité est importante, elle est capée pas le calcul des maximums locaux (étape la plus longue). car on parcours tous les cercles possibles, sur xCenter, yCenter, rad (N^3), et pour ces cercles on va comparer avec les 120 voisins, on monte donc environ à N^4.

<img src="./res/x4.png" width="300">

____  


## Ouvertures
* Effectuer la détection sur les différents canaux de couleur au lieu de le faire sur l'image en noir et blanc;
* l'espace de recherche des maximums locaux (fixé aux 120 voisins devrait être déduit proportionnellement à la taille de l'image;
