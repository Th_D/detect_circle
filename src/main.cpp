#include <iostream>
#include <stdio.h>
#include <math.h>
#include <chrono>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

#define GAUSSIAUN_BLUR_SIZE 3,3
#define MAX_RADIUS getDist(0,0,image_grey.rows, image_grey.cols)

// r and c are discretized according:
#define DELTA_R 1
#define DELTA_C 1

struct detected_circle {
	int i, j, rad;
	int score;
};

float getDist(int xa, int ya, int xb, int yb)
{
	return (sqrt( ((xb-xa)*(xb-xa)) + ((yb-ya)*(yb-ya))));
}

void addAllCircles(uint pos_r, uint pos_c, vector<vector<vector<uint>>> &circles, int sizeImg_r, int sizeImg_c)
{
	for(int i=0; i<sizeImg_r; i++) //r
	{
		for(int j=0; j<sizeImg_c; j++) //c
		{
			circles[i/DELTA_R][j/DELTA_C][(int)(getDist(i, j, pos_r, pos_c))]++;
			//cout<<(int)(getDist(i, j, pos_r, pos_c)/DELTA_RAD)<<'\n';
		}
	}
}

void cleanForCenter(vector<uint> &center)
{
	uint max = *max_element(center.begin(), center.end());
	for(int i = 0; i<center.size(); i++)
	{
		if(center[i]!=max)
		{
			center[i] = 0;
		}
	}
}

//search for local maximum and remove double circles
void cleanCircles(vector<vector<vector<uint>>> &circles, vector<vector<vector<int>>> &isMax)
{
	for(int i=2; i<circles.size()-2; i++) //r
	{
		for(int j=2; j<circles[0].size()-2; j++) //c
		{//			cleanForCenter(circles[i][j]);
			for(int k=2; k<circles[0][0].size()-2; k++)
			{
				uint max=0;
				for(int ii=i-2; ii<i+3; ii++) //r
				{
					for(int jj=j-2; jj<j+3; jj++) //c
					{
						//cleanForCenter(circles[i][j]);
						for(int kk=k-2; kk<k+3; kk++)
						{
							if(circles[ii][jj][kk] > max)
							{
								max = circles[ii][jj][kk];
							}
						}
					}
				}

				for(int ii=i-2; ii<i+3; ii++) //r
				{
					for(int jj=j-2; jj<j+3; jj++) //c
					{//			cleanForCenter(circles[i][j]);
						for(int kk=k-2; kk<k+3; kk++)
						{
							if(circles[ii][jj][kk] == max)
							{
								//cout<<"max"<<std::endl;
								isMax[ii][jj][kk]++;
							}
							else
							{
								isMax[ii][jj][kk]--;
							}
							//cout<<"min "<<std::endl;
						}
					}
				}
			}
		}
	}
}

int getThreshold(Mat mat)
{
	/*
	int max = 0;
	double ave = 0;
	int nbval = (mat.rows * mat.cols);
	for(int i=0; i<mat.rows; i++) //r
	{
	for(int j=0; j<mat.cols; j++) //c
	{
	ave += (float)(mat.at<uchar>(i,j)) / nbval;
	if(mat.at<uchar>(i,j)>max)
	{
	max = mat.at<uchar>(i,j);
}
}
}
*/
return 150;
//cout<<"getThreshold === "<<(int)(ave + max * 0.4)<<std::endl;
//return (int)(ave + max * 0.5);
}

//on compare par rapport au score / perimetre du cercle (= 2 x pi x r ~= 6r)
float getRadProp(detected_circle c)
{
	return (c.score / (2.*M_PI*c.rad));
}

//ne garder que les N meilleurs cercles
void add_if_top(vector<detected_circle> *list, detected_circle c, int nb_circles)
{
	//std::cout<<"SIZE ===="<<c.score<<std::endl;
	if(list->size()<nb_circles)
	{
		list->push_back(c);
	} else {
		float min = 999;
		int idmin = -1;
		for(int i = 0; i < nb_circles; i++)
		{
			if(getRadProp(list->at(i)) < min)
			{
				min = getRadProp(list->at(i));
				idmin = i;
				//cout<<"min = "<<min<<"idmin = "<<idmin<<"\n";
			}
		}
		if(idmin != -1 && getRadProp(c) > min)
		{
			//cout<<"on vire = "<<list->at(idmin).score<<"  getRadProp = "<<getRadProp(list->at(idmin))<<"\n";
			//cout<<"on add = "<<c.score<<"  getRadProp = "<<getRadProp(c)<<"\n";

			list->erase(list->begin()+idmin);
			list->push_back(c);

			//std::cout<<c.score<<endl;
		}
	}
}

int main(int argc, char** argv){
	chrono::milliseconds start = chrono::duration_cast< chrono::milliseconds >(
		chrono::system_clock::now().time_since_epoch()
	);

	// try yo open the file specified in args
	cv::Mat image_grey;
	cv::Mat image_color;

	int N = 5; // number of circles to detect
	int MIN_RAD = 4;

	if(argc >= 2)
	{
		image_grey = cv::imread( argv[1], IMREAD_GRAYSCALE);
		image_color = cv::imread( argv[1], IMREAD_COLOR);
	}
	if(argc >= 3)
	{
		N = atoi(argv[2]);
	}
	if(argc == 4)
	{
		MIN_RAD = atoi(argv[3]);
	}
	if(argc < 2 || argc > 4) {
		std::cerr<<"Error: invalid use : \""<<argv[0]<<"\""<<std::endl;
		std::cerr<<"Error: try \""<<argv[0]<<" path_to_img_file\""<<std::endl;
		return -1;
	}

	// Check input
	if(! image_grey.data ) /// check for invalid input
	{
		std::cerr<<"Error: Could not open or find the image_grey"<<std::endl;
		std::cerr<<"Error: check if ["<<argv[1]<<"] is a correct image_grey file"<<std::endl;
		return -1;
	}

	std::cout<<"Apply Gaussian blur..."<<std::endl;
	// Blur the image_grey to reduce the noise
	cv::GaussianBlur(image_grey, image_grey, cv::Size(GAUSSIAUN_BLUR_SIZE), 0);
	std::cout<<"\t\t...done"<<std::endl;

	std::cout<<"Apply Sobel derivate..."<<std::endl;
	// get the borders using the Sobel derivates
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;
	Mat grad;

	// Sobel on X
	Sobel( image_grey, grad_x, CV_16S, 1, 0, 3);

	convertScaleAbs( grad_x, abs_grad_x );
	// Sobel on Y
	Sobel( image_grey, grad_y, CV_16S, 0, 1, 3);
	convertScaleAbs( grad_y, abs_grad_y );
	// Combination of the 2 gradients
	addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad ); //pas ok ?
	std::cout<<"\t\t...done"<<std::endl;

	// a circle is defined wth a center [r, c] and a radius
	/// each cell of circles correspond to a circle
	//	vector<vector<vector<int>(MAX_RADIUS/DELTA_RAD, 0)>(image_grey.cols/DELTA_C)>(image_grey.rows/DELTA_R) circles;

	std::cout<<"Compute gradient's threshold..."<<endl;
	vector<vector<vector<uint>>> circles (image_grey.rows/DELTA_R, vector<vector<uint>>(image_grey.cols/DELTA_C, vector<uint>(MAX_RADIUS, 0)));
	vector<vector<vector<int>>> circles_max (image_grey.rows/DELTA_R, vector<vector<int>>(image_grey.cols/DELTA_C, vector<int>(MAX_RADIUS, 0)));
	std::cout<<"\t\t...done"<<std::endl;

	//Mat circles(3, size, CV_16UC1, cv::Scalar(0));
	int border_threshold = getThreshold(grad);

	std::cout<<"Register all circles..."<<endl;
	for(int i=0; i<grad.rows; i++) //r
	{
		for(int j=0; j<grad.cols; j++) //c
		{
			if (grad.at<uchar>(i,j) > border_threshold)
			{
				// cout<<(int)image_grey.at<uchar>(i,j)<<endl;
				addAllCircles(i, j, circles, grad.rows, grad.cols);
				image_grey.at<uchar>(i,j) = (uchar)100;
			}
		}
	}
	std::cout<<"\t\t...done"<<std::endl;


	std::cout<<"Compute local maximum..."<<endl;
	cleanCircles(circles, circles_max);
	std::cout<<"\t\t...done"<<std::endl;

	std::cout<<"Select better circles..."<<endl;
	vector<detected_circle> *top_circles = new vector<detected_circle>(0);

	for(int r = 0; r < circles.size(); r++)
	{
		for(int c = 0; c < circles[0].size(); c++)
		{
			for(int rad = MIN_RAD; rad < circles[0][0].size(); rad++)
			{
				//2xpixr ~= 6xr

				if(circles_max[r][c][rad]>=125){
					//cout<<"MAAAX = "<< circles_max[r][c][rad];
					//circles[r][c][rad]>(4*rad * DELTA_C * DELTA_R) &&
					add_if_top(top_circles, {r, c, rad, circles[r][c][rad]}, N);
				}
			}
		}
	}
	std::cout<<"\t\t...done"<<std::endl;

	std::cout<<"Draw circles..."<<endl;

	for(int i=0; i<top_circles->size() ; i++) {
		cout<<"\t"<<top_circles->at(i).i<<','<<top_circles->at(i).j<<' '<<top_circles->at(i).rad<<"->"<< circles[top_circles->at(i).i][top_circles->at(i).j][top_circles->at(i).rad]<<"["<<circles_max[top_circles->at(i).i][top_circles->at(i).j][top_circles->at(i).rad]<<"]"<<endl;

		// Draw circle
		circle(image_color, Point(top_circles->at(i).j*DELTA_C, top_circles->at(i).i*DELTA_R), top_circles->at(i).rad, Scalar(0,0,255));
		// Draw center
		image_color.at<Vec3b>(top_circles->at(i).i,top_circles->at(i).j) = Vec3b(255,0,0);
		image_color.at<Vec3b>(top_circles->at(i).i-1,top_circles->at(i).j) = Vec3b(255,0,0);
		image_color.at<Vec3b>(top_circles->at(i).i+1,top_circles->at(i).j) = Vec3b(255,0,0);
		image_color.at<Vec3b>(top_circles->at(i).i,top_circles->at(i).j-1) = Vec3b(255,0,0);
		image_color.at<Vec3b>(top_circles->at(i).i,top_circles->at(i).j+1) = Vec3b(255,0,0);
	}
	std::cout<<"\t\t...done"<<std::endl;

	chrono::milliseconds end = chrono::duration_cast< chrono::milliseconds >(
		chrono::system_clock::now().time_since_epoch()
	);


	auto duration = chrono::duration_cast<chrono::milliseconds>(end - start).count() / 1000.;
	cout<<"\nProcessing of circle detection took "<<duration<<" seconds\n"<<endl;

	cv::imshow( "Sobel on image_grey", image_grey );
	cv::imshow( "Circles on image_color", image_color );
	cv::imshow( "Grad", grad );
	cv::waitKey(0);


	return 0;
}
